/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: josaykos <josaykos@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/14 13:45:57 by josaykos          #+#    #+#             */
/*   Updated: 2019/11/14 17:09:25 by josaykos         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "libft.h"

int		main(void)
{
	t_list **list;
	t_list *head;
	t_list *new_node;

	list = NULL;
	head = ft_lstnew("1st node created");
	list = &head;

	printf(">>>>>>>>>> head node: [%s]\n", head->content); 
	printf(">> next node address: |%p|\n\n", head->next);

	new_node = ft_lstnew("2nd node created, add to front");

	ft_lstadd_front(list, new_node);

	printf(">>>>>  new head node: [%s]\n", head->content); 
	printf(">>> next node adress: |%p|\n", head->next);
	printf("content in next node: [%s]\n", head->next->content);

	return (0);
}
