/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: josaykos <josaykos@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/05 11:19:21 by josaykos          #+#    #+#             */
/*   Updated: 2019/11/05 19:54:21 by jonny            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <libft.h>

int		main(int argc, char **argv)
{
	if (argc == 3)
	{
		char *str1 = argv[1];
		char *str2 = argv[1];
		unsigned int nb = atoi(argv[2]);
		
		printf("Before, str = %s\n", str1);
		bzero(str1, nb);
		printf(">> After bzero, str: %s\n", str1);
		printf(">> After bzero, str[0]: %c\n", str1[0]);
		printf(">> After bzero, str[1]: %c\n", str1[1]);
		printf(">> After bzero, str[2]: %c\n", str1[2]);
		printf(">> After bzero, str[3]: %c\n", str1[3]);
		printf(">> After bzero, str[4]: %c\n", str1[4]);
		printf("\n");
		ft_bzero(str2, nb);
		printf("After ft_bzero, str: %s\n", str2);
		printf("After ft_bzero, str[0]: %c\n", str2[0]);
		printf("After ft_bzero, str[1]: %c\n", str2[1]);
		printf("After ft_bzero, str[2]: %c\n", str2[2]);
		printf("After ft_bzero, str[3]: %c\n", str2[3]);
		printf("After ft_bzero, str[4]: %c\n", str2[4]);
	}
	else
		printf("Usage: ./a.out [string] [nb bytes]\n");
	return(0);
}
