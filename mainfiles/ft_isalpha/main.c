/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jonny <josaykos@student.42.fr>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/07 16:54:34 by jonny             #+#    #+#             */
/*   Updated: 2019/11/07 17:17:33 by jonny            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <libft.h>

int		main(int argc, char **argv)
{
	int character = argv[1][0];

	if (argc == 2)
	{
		if (ft_isalpha(character))
			printf("The character |%c| is an alphabet\n", character);
		else
			printf("The character |%c| is NOT an alphabet\n", character);
	}
	else
		printf("Usage: ./a.out [character]\n");
	return (0);
}
