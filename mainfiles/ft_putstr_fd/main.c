/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: josaykos <josaykos@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/14 11:01:07 by josaykos          #+#    #+#             */
/*   Updated: 2019/11/14 12:14:32 by josaykos         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <fcntl.h>
#include "libft.h"

int		main(int argc, char **argv)
{
	char *str;
	int fd;

	if (argc == 2)
	{
		str = argv[1];

		fd = open("./TEST", O_WRONLY | O_APPEND);
		ft_putstr_fd(str, fd);
		close(fd);
	}
	return (0);
}
