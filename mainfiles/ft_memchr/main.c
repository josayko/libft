/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: josaykos <josaykos@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/06 13:56:20 by josaykos          #+#    #+#             */
/*   Updated: 2019/11/06 14:43:46 by josaykos         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libft.h>

int		main(int argc, char **argv)
{
	(void)argc;

	const char *s1 = argv[1];
	const char s2 = argv[2][0];
	char *ret;

//	ret = memchr(s1, s2, strlen(s1));
	ret = ft_memchr(s1, s2, strlen(s1));
//	printf("memchr --> String after |%c| is - |%s|\n", s2, ret);
	printf("ft_memchr --> String after |%c| is - |%s|\n", s2, ret);
	return (0);
}
