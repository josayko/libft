/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: josaykos <josaykos@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/08 10:00:12 by josaykos          #+#    #+#             */
/*   Updated: 2019/11/08 10:21:17 by josaykos         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "libft.h"

int		main(int argc, char **argv)
{
	if (argc == 2)
	{
		unsigned char character = argv[1][0];

		if (ft_isprint(character))
			printf("The character |%c| is printable\n", character);
		else
			printf("The character |%c| is NOT printable\n", character);
	}
	else
	{
		printf("Usage: ./a.out [character]\n");
	}
	return (0);
}
