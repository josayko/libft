/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: josaykos <josaykos@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/04 14:13:56 by josaykos          #+#    #+#             */
/*   Updated: 2019/11/09 11:01:57 by josaykos         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "libft.h"

int		main(int argc, char **argv)
{
	if (argc == 4)
	{
		char *str1 = argv[1];
		const char *str2 = argv[2];

		printf(">>> Before copy, dest = %s\n", str1);

//		memcpy(str1, str2, atoi(argv[3]));
//		printf(">> After memcpy, dest = %s\n", str1);

		ft_memcpy(str1, str2, atoi(argv[3]));
		printf("After ft_memcpy, dest = %s\n", str1);
	}
	else
	{
		printf("Usage: ./a.out [destination] [source] [nb bytes]\n");
	}
	return (0);
}
