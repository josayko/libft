/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: josaykos <josaykos@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/12 09:28:08 by josaykos          #+#    #+#             */
/*   Updated: 2019/11/12 09:54:18 by josaykos         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>
#include "libft.h"

int		main(int argc, char **argv)
{
	(void)argc;
	char *str = argv[1];

	printf("atoi : %d\n", atoi(str));
	printf("ft_atoi : %d\n", ft_atoi(str));

	return (0);
}
