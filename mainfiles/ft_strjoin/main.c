/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: josaykos <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/12 11:51:41 by josaykos          #+#    #+#             */
/*   Updated: 2019/11/12 11:40:10 by josaykos         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "libft.h"

int		main(void)
{
	char *str1 = "Hello";
	char *str2 = "World";
	char *join_params;
	join_params = ft_strjoin(str1, str2);
	printf("%s", join_params);
}
