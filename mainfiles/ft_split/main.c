/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: josaykos <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/18 12:09:06 by josaykos          #+#    #+#             */
/*   Updated: 2019/11/15 17:03:27 by josaykos         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "libft.h"

int		main()
{
	int i;

	char *str1 = "Hello how are you ?";
	char ch = ' ';
	char **tab;

	tab = ft_split(str1, ch);
	i = 0;
	while(tab[i])
	{
		printf("%s\n", tab[i]);
		i++;
	}
	return (0);
}
