/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: josaykos <josaykos@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/13 11:27:31 by josaykos          #+#    #+#             */
/*   Updated: 2019/11/21 11:41:45 by josaykos         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "libft.h"

int		main(void)
{
	char * ret;
	const char *str1 = "";
	unsigned int index = 0;
	size_t n = 1;

	ret = ft_substr(str1, index, n);
	printf("result: %s\n", ret);
	return (0);
}
