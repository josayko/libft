/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: josaykos <josaykos@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/08 10:51:34 by josaykos          #+#    #+#             */
/*   Updated: 2019/11/12 09:07:30 by josaykos         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <string.h>
#include "libft.h"

int		main(void)
{
		char *ret;
		char *str = "bonjour";
		
	//	ret = strchr(str, '\0');
		ret = ft_strchr("str", '\0');
		
		printf("String after |%s| is - |%s|\n", str, ret);
	return (0);
}
