/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: josaykos <josaykos@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/05 11:39:02 by josaykos          #+#    #+#             */
/*   Updated: 2019/11/06 13:39:46 by josaykos         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libft.h>

int		main(int argc, char **argv)
{
	if (argc == 3)
	{
		char *str1 = argv[1];

//		printf(">> Before memmove, dest: %s\n", str1);
//		memmove(str1+3, str1, atoi(argv[2]));
//		printf(">>> After memmove, dest: %s\n", str1);

		printf("Before ft_memmove, dest: %s\n", str1);
		ft_memmove(str1+3, str1, atoi(argv[2]));
		printf(" After ft_memmove, dest: %s\n", str1);
	}
	else
		printf("Usage: ./a.out [buffer] [nb bytes]\n");
	return (0);
}
