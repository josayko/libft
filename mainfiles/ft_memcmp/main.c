/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: josaykos <josaykos@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/06 15:20:42 by josaykos          #+#    #+#             */
/*   Updated: 2019/11/08 17:04:17 by josaykos         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libft.h>

int		main(int argc, char **argv)
{
	(void)argc;

	char str1[25] = "\0";
	char str2[25] = "\0";
	int ret;

	//ret = memcmp(str1, str2, atoi(argv[3]));
	ret = ft_memcmp(str1, str2, atoi(argv[1]));

	if(ret > 0)
		printf("str2 is less than str1");
	else if(ret < 0)
		printf("str1 is less than str2");
	else
		printf("str1 is equal to str2");

	return(0);
}
