/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: josaykos <josaykos@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/08 17:10:32 by josaykos          #+#    #+#             */
/*   Updated: 2019/11/08 20:24:11 by jonny            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

unsigned int	ft_strlcpy(char *dest, char *src, unsigned int size);

int		main(int argc, char *argv[])
{
	if (argc == 4)
	{
		//printf("%lu\n", strlcpy(argv[1], argv[2], atoi(argv[3])));
		printf("%u\n", ft_strlcpy(argv[1], argv[2], atoi(argv[3])));
	}
	else
		return (0);
}
