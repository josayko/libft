/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: josaykos <josaykos@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/04 18:05:44 by josaykos          #+#    #+#             */
/*   Updated: 2019/11/06 10:59:51 by josaykos         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libft.h>

int		main(int argc, char **argv)
{
	if (argc == 3)
	{
		char *buffer = argv[1];
		const char *str = argv[2];

//		printf(">>> Before memcpy, buffer = %s\n", buffer);
//		memccpy(buffer, str, ':', strlen(buffer) + 1);
//		printf(">>> After memccpy, buffer = %s\n", buffer);

		printf("Before ft_memccpy, buffer = %s\n", buffer);
		ft_memccpy(buffer, str, ':', strlen(buffer) + 1);
		printf(" After ft_memccpy, buffer = %s\n", buffer);
	}
	else
		printf("Usage: ./a.out [buffer] [string]\n");
	return (0);
}
