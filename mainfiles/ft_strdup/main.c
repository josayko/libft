/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: josaykos <josaykos@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/12 10:23:13 by josaykos          #+#    #+#             */
/*   Updated: 2019/11/12 10:36:58 by josaykos         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <string.h>
#include "libft.h"

int main()
{
	char *str = "Helloworld";
	char *result;

	//result = strdup(str);
	result = ft_strdup(str);
	printf("The string : %s", result);
	return 0;
}
