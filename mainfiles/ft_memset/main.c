/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: josaykos <josaykos@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/04 14:54:46 by josaykos          #+#    #+#             */
/*   Updated: 2019/11/05 19:55:26 by jonny            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libft.h>

int		main(int argc, char **argv)
{
	if (argc == 3)
	{
		char *str1;
		char *str2;

		printf(">>>>>>>>>: %s\n", argv[1]);

		str1 = memset(argv[1], '$', atoi(argv[2]));
		str2 = ft_memset(argv[1], '$', atoi(argv[2]));
		printf(">> memset: %s\n", str1);
		printf("ft_memset: %s\n", str2);
	}
	else
	{
		printf("Usage: ./a.out [string] [nb bytes]\n");
	}
	return (0);
}
