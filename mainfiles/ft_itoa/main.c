/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: josaykos <josaykos@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/13 10:30:07 by josaykos          #+#    #+#             */
/*   Updated: 2019/11/13 11:13:07 by josaykos         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "libft.h"

int		main(int argc, char **argv)
{
	int number = atoi(argv[1]);
	char *ret;

	if (argc == 2)
	{
		ret = ft_itoa(number);
		printf("result: %s\n", ret);
	}
	return (0);
}
