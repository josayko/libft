/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jonny <josaykos@student.42.fr>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/07 20:07:30 by jonny             #+#    #+#             */
/*   Updated: 2019/11/08 09:57:59 by josaykos         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "libft.h"

int		main(int argc, char **argv)
{
	char character = argv[1][0];
	
	if (argc == 2)
	{
		if (ft_isascii(character))
		{
			printf("The character |%c| is an ASCII character\n", character);
		}
		else
			printf("The character |%c| is NOT an ASCII character\n", character);
	}
	else
		printf("Usage: ./a.out [character]\n");
}
