/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: josaykos <josaykos@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/08 10:51:34 by josaykos          #+#    #+#             */
/*   Updated: 2019/11/08 15:05:56 by josaykos         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <string.h>
#include "libft.h"

int		main(int argc, char **argv)
{
	if (argc == 3)
	{
		char *str;
		char ch;
		char *ret;
		
		str = argv[1];
		ch = argv[2][0];

	//	ret = strrchr(str, ch);
		ret = ft_strrchr(str, ch);
		
		printf("String after last |%c| is - |%s|\n", ch, ret);
	}	
	else
		printf("Usage: ./a.out [string] [character]\n");
	return (0);
}
