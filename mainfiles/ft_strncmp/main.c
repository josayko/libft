/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: josaykos <josaykos@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/08 16:26:57 by josaykos          #+#    #+#             */
/*   Updated: 2019/11/08 16:45:33 by josaykos         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>
#include "libft.h"

int		main(int argc, char **argv)
{
	int ret;

	if (argc == 4)
	{
		ret = ft_strncmp(argv[1], argv[2], atoi(argv[3]));
		if (ret < 0)
			printf("str1 is less than str2\n");
		else if (ret > 0)
			printf("str2 is less than str1\n");
		else
			printf("str1 is equal to str2\n");
	}
	else
		printf("Usage: ./a.out [str1] [str2] [nb bytes]\n");
	return (0);
}
