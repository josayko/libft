/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: josaykos <josaykos@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/09 14:06:14 by josaykos          #+#    #+#             */
/*   Updated: 2019/11/14 13:39:59 by josaykos         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "libft.h"

int main () 
{
	const char haystack[50] = "lorem ipsum dolor sit amet";
	const char needle[10] = "dolor";
	char *ret;
	unsigned int n = 16;

	//ret = strnstr(haystack, needle, n);
	ret = ft_strnstr(haystack, needle, n);

	printf("The substring is: %s\n", ret);

	return(0);
}
