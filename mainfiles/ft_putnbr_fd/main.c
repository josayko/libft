/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: josaykos <josaykos@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/14 11:01:07 by josaykos          #+#    #+#             */
/*   Updated: 2019/11/14 13:12:04 by josaykos         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <fcntl.h>
#include "libft.h"

int		main(int argc, char **argv)
{
	int nb = atoi(argv[1]);	
	int fd;

	if (argc == 2)
	{
		fd = open("./TEST", O_WRONLY | O_APPEND);
		ft_putnbr_fd(nb, fd);
		close(fd);
	}
	return (0);
}
