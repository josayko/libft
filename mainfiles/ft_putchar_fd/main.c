/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: josaykos <josaykos@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/14 11:01:07 by josaykos          #+#    #+#             */
/*   Updated: 2019/11/14 11:49:41 by josaykos         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <fcntl.h>
#include "libft.h"

int		main(int argc, char **argv)
{
	char ch;
	char buf[1024];
	int fd;

	if (argc == 2)
	{
		ch = argv[1][0];

		fd = open("./TEST", O_WRONLY);
		ft_putchar_fd(ch, fd);
		close(fd);
		
		fd = open("./TEST", O_RDONLY);
		while (read(fd, buf, 1) != 0)
			printf("%s", buf);
		close(fd);

		return (0);
	}
}
