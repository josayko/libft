/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: josaykos <josaykos@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/09 12:03:02 by josaykos          #+#    #+#             */
/*   Updated: 2019/11/09 12:51:34 by josaykos         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "libft.h"

int		main(void)
{

	char str1[50];
	char str2[50];
	unsigned int n = 8;

	strcpy(str1, "Salut");
	strcpy(str2, "Hello");
	//printf("%zu\n", strlcat(str1, str2, n));
	printf("%zu\n", ft_strlcat(str1, str2, n));
	printf("%s\n", str1);
	return (0);
}
