/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: josaykos <josaykos@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/14 13:45:57 by josaykos          #+#    #+#             */
/*   Updated: 2019/11/14 14:10:42 by josaykos         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "libft.h"

int		main(void)
{
	t_list *list;

	list = ft_lstnew("Hello");

	printf("list content: [%s]\n", list->content); 
	printf(">>>>>>> next: |%p|\n", list->next);
	return (0);
}
