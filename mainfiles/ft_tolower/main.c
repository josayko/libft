/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: josaykos <josaykos@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/08 10:31:09 by josaykos          #+#    #+#             */
/*   Updated: 2019/11/08 10:46:53 by josaykos         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

# include <stdio.h>
# include "libft.h"

int		main(int argc, char **argv)
{
	if (argc == 2)
	{
		unsigned char character = ft_tolower(argv[1][0]);

		printf("%c\n", character);
	}
	else
		printf("Usage: ./a.out [character]\n");
	return (0);
}
