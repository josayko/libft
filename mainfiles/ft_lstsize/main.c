/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: josaykos <josaykos@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/14 13:45:57 by josaykos          #+#    #+#             */
/*   Updated: 2019/11/15 11:09:00 by josaykos         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "libft.h"

int		main(void)
{
	t_list **head;
	t_list *node;
	t_list *new_node;

	head = NULL;
	node = ft_lstnew("1st node created");
	head = &node;
	printf("********************************************************\n");
	printf(">>>>>>>>>> head node: [%s]\n", node->content); 
	printf(">> next node address: |%p|\n\n", node->next);

	new_node = ft_lstnew("2nd node created, add to front");
	ft_lstadd_front(head, new_node);
	printf(">>>>>  new head node: [%s]\n", node->content); 
	printf(">>> next node adress: |%p|\n", node->next);
	printf("content in next node: [%s]\n\n", node->next->content);

	printf("********************************************************\n");
	printf("> total nb of nodes : |%d|\n", ft_lstsize(new_node));


	return (0);
}
